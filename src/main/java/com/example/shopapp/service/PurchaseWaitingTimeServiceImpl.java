package com.example.shopapp.service;

import com.example.shopapp.dto.PurchaseWaitingTimeDto;
import com.example.shopapp.service.interfaces.PurchaseWaitingTimeService;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public class PurchaseWaitingTimeServiceImpl implements PurchaseWaitingTimeService {
    @Override
    public HttpEntity getAllPurchaseWaitingTime(int size, int page, String search, String sort, boolean direction) {
        return null;
    }

    @Override
    public HttpEntity getPurchaseWaitingTimeById(UUID id) {
        return null;
    }

    @Override
    public HttpEntity savePurchaseWaitingTime(PurchaseWaitingTimeDto purchaseWaitingTime) {
        return null;
    }

    @Override
    public void deletePurchaseWaitingTimeById(UUID id) {

    }
}
