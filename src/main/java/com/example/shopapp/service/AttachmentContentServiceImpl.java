package com.example.shopapp.service;

import com.example.shopapp.dto.AttachmentContentDto;
import com.example.shopapp.service.interfaces.AttachmentContentService;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public class AttachmentContentServiceImpl implements AttachmentContentService {
    @Override
    public HttpEntity getAllAttachmentContent(int size, int page, String search, String sort, boolean direction) {
        return null;
    }

    @Override
    public HttpEntity getAttachmentContentById(UUID id) {
        return null;
    }

    @Override
    public HttpEntity saveAttachmentContent(AttachmentContentDto attachmentContent) {
        return null;
    }

    @Override
    public void deleteAttachmentContentById(UUID id) {

    }
}
