package com.example.shopapp.service;

import com.example.shopapp.dto.DeleteTimeDto;
import com.example.shopapp.service.interfaces.DeleteTimeService;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public class DeleteTimeServiceImpl implements DeleteTimeService {
    @Override
    public HttpEntity getAllDeleteTime(int size, int page, String search, String sort, boolean direction) {
        return null;
    }

    @Override
    public HttpEntity getDeleteTimeById(UUID id) {
        return null;
    }

    @Override
    public HttpEntity saveDeleteTime(DeleteTimeDto deleteTime) {
        return null;
    }

    @Override
    public void deleteDeleteTimeById(UUID id) {

    }
}
