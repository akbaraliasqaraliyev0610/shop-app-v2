package com.example.shopapp.service;

import com.example.shopapp.dto.StructureDto;
import com.example.shopapp.service.interfaces.StructureService;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public class StructureServiceImpl implements StructureService {
    @Override
    public HttpEntity getAllStructure(int size, int page, String search, String sort, boolean direction) {
        return null;
    }

    @Override
    public HttpEntity getStructureById(UUID id) {
        return null;
    }

    @Override
    public HttpEntity saveStructure(StructureDto structure) {
        return null;
    }

    @Override
    public void deleteStructureById(UUID id) {

    }
}
