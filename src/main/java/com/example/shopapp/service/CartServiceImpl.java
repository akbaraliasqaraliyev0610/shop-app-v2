package com.example.shopapp.service;

import com.example.shopapp.CartDto;
import com.example.shopapp.service.interfaces.CartService;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public class CartServiceImpl implements CartService {
    @Override
    public HttpEntity getAllCart(int size, int page, String search, String sort, boolean direction) {
        return null;
    }

    @Override
    public HttpEntity getCartById(UUID id) {
        return null;
    }

    @Override
    public HttpEntity saveCart(CartDto cart) {
        return null;
    }

    @Override
    public void deleteCartById(UUID id) {

    }
}
