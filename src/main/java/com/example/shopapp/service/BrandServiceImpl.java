package com.example.shopapp.service;

import com.example.shopapp.dto.BrandDto;
import com.example.shopapp.service.interfaces.BrandService;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public class BrandServiceImpl implements BrandService {
    @Override
    public HttpEntity getAllBrand(int size, int page, String search, String sort, boolean direction) {
        return null;
    }

    @Override
    public HttpEntity getBrandById(UUID id) {
        return null;
    }

    @Override
    public HttpEntity saveBrand(BrandDto brand) {
        return null;
    }

    @Override
    public void deleteBrandById(UUID id) {

    }
}
