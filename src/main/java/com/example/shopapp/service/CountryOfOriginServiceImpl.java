package com.example.shopapp.service;

import com.example.shopapp.dto.CountryOfOriginDto;
import com.example.shopapp.service.interfaces.CountryOfOriginService;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public class CountryOfOriginServiceImpl implements CountryOfOriginService {
    @Override
    public HttpEntity getAllCountryOfOrigin(int size, int page, String search, String sort, boolean direction) {
        return null;
    }

    @Override
    public HttpEntity getCountryOfOriginById(UUID id) {
        return null;
    }

    @Override
    public HttpEntity saveCountryOfOrigin(CountryOfOriginDto countryOfOrigin) {
        return null;
    }

    @Override
    public void deleteCountryOfOriginById(UUID id) {

    }
}
