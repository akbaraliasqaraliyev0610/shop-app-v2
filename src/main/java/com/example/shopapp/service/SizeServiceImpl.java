package com.example.shopapp.service;

import com.example.shopapp.dto.SizeDto;
import com.example.shopapp.service.interfaces.SizeService;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public class SizeServiceImpl implements SizeService {
    @Override
    public HttpEntity getAllSize(int size, int page, String search, String sort, boolean direction) {
        return null;
    }

    @Override
    public HttpEntity getSizeById(UUID id) {
        return null;
    }

    @Override
    public HttpEntity saveSize(SizeDto size) {
        return null;
    }

    @Override
    public void deleteSizeById(UUID id) {

    }
}
