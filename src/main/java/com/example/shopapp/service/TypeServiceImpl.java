package com.example.shopapp.service;

import com.example.shopapp.dto.TypeDto;
import com.example.shopapp.service.interfaces.TypeService;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public class TypeServiceImpl implements TypeService {
    @Override
    public HttpEntity getAllType(int size, int page, String search, String sort, boolean direction) {
        return null;
    }

    @Override
    public HttpEntity getTypeById(UUID id) {
        return null;
    }

    @Override
    public HttpEntity saveType(TypeDto type) {
        return null;
    }

    @Override
    public void deleteTypeById(UUID id) {

    }
}
