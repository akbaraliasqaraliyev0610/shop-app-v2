package com.example.shopapp.service;

import com.example.shopapp.dto.ClothDto;
import com.example.shopapp.service.interfaces.ClothService;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public class ClothServiceImpl implements ClothService {
    @Override
    public HttpEntity getAllCloth(int size, int page, String search, String sort, boolean direction) {
        return null;
    }

    @Override
    public HttpEntity getClothById(UUID id) {
        return null;
    }

    @Override
    public HttpEntity saveCloth(ClothDto cloth) {
        return null;
    }

    @Override
    public void deleteClothById(UUID id) {

    }
}
