package com.example.shopapp.service;

import com.example.shopapp.dto.AttachmentDto;
import com.example.shopapp.service.interfaces.AttachmentService;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public class AttachmentServiceImpl implements AttachmentService {
    @Override
    public HttpEntity getAllAttachment(int size, int page, String search, String sort, boolean direction) {
        return null;
    }

    @Override
    public HttpEntity getAttachmentById(UUID id) {
        return null;
    }

    @Override
    public HttpEntity saveAttachment(AttachmentDto attachment) {
        return null;
    }

    @Override
    public void deleteAttachmentById(UUID id) {

    }
}
