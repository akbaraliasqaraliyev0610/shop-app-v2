package com.example.shopapp.service.interfaces;

import com.example.shopapp.dto.ClothDto;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface ClothService {
    HttpEntity getAllCloth(int size, int page, String search, String sort, boolean direction);

    HttpEntity getClothById(UUID id);

    HttpEntity saveCloth(ClothDto cloth);

    void deleteClothById(UUID id);
}
