package com.example.shopapp.service.interfaces;

import com.example.shopapp.dto.DiscountDto;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface DiscountService {
    HttpEntity getAllDiscount(int size, int page, String search, String sort, boolean direction);

    HttpEntity getDiscountById(UUID id);

    HttpEntity saveDiscount(DiscountDto discount);

    void deleteDiscountById(UUID id);
}
