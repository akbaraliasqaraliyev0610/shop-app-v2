package com.example.shopapp.service.interfaces;

import com.example.shopapp.dto.AgeDto;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface AgeService {
    HttpEntity getAllAge(int size, int page, String search, String sort, boolean direction);

    HttpEntity getAgeById(UUID id);

    HttpEntity saveAge(AgeDto age);

    void deleteAgeById(UUID id);
}
