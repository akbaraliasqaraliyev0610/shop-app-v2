package com.example.shopapp.service.interfaces;

import com.example.shopapp.dto.SizeDto;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface SizeService {
    HttpEntity getAllSize(int size, int page, String search, String sort, boolean direction);

    HttpEntity getSizeById(UUID id);

    HttpEntity saveSize(SizeDto size);

    void deleteSizeById(UUID id);
}
