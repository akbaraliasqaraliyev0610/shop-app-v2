package com.example.shopapp.service.interfaces;

import com.example.shopapp.dto.SendConfirmPasswordTImeDto;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface SendConfirmPasswordTimeService {
    HttpEntity getAllSendConfirmPasswordTIme(int size, int page, String search, String sort, boolean direction);

    HttpEntity getSendConfirmPasswordTImeById(UUID id);

    HttpEntity saveSendConfirmPasswordTIme(SendConfirmPasswordTImeDto sendConfirmPasswordTIme);

    void deleteSendConfirmPasswordTImeById(UUID id);
}
