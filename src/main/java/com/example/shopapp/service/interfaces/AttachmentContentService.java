package com.example.shopapp.service.interfaces;

import com.example.shopapp.dto.AttachmentContentDto;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface AttachmentContentService {
    HttpEntity getAllAttachmentContent(int size, int page, String search, String sort, boolean direction);

    HttpEntity getAttachmentContentById(UUID id);

    HttpEntity saveAttachmentContent(AttachmentContentDto attachmentContent);

    void deleteAttachmentContentById(UUID id);
}
