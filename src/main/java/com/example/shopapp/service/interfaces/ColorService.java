package com.example.shopapp.service.interfaces;

import com.example.shopapp.dto.ColorDto;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface ColorService {
    HttpEntity getAllColor(int size, int page, String search, String sort, boolean direction);

    HttpEntity getColorById(UUID id);

    HttpEntity saveColor(ColorDto color);

    void deleteColorById(UUID id);
}
