package com.example.shopapp.service.interfaces;

import com.example.shopapp.dto.StructureDto;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface StructureService {
    HttpEntity getAllStructure(int size, int page, String search, String sort, boolean direction);

    HttpEntity getStructureById(UUID id);

    HttpEntity saveStructure(StructureDto structure);

    void deleteStructureById(UUID id);
}
