package com.example.shopapp.service.interfaces;

import com.example.shopapp.dto.CountryOfOriginDto;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface CountryOfOriginService {
    HttpEntity getAllCountryOfOrigin(int size, int page, String search, String sort, boolean direction);

    HttpEntity getCountryOfOriginById(UUID id);

    HttpEntity saveCountryOfOrigin(CountryOfOriginDto countryOfOrigin);

    void deleteCountryOfOriginById(UUID id);
}
