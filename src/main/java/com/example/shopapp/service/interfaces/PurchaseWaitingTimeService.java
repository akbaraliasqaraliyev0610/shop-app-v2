package com.example.shopapp.service.interfaces;

import com.example.shopapp.dto.PurchaseWaitingTimeDto;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface PurchaseWaitingTimeService {
    HttpEntity getAllPurchaseWaitingTime(int size, int page, String search, String sort, boolean direction);

    HttpEntity getPurchaseWaitingTimeById(UUID id);

    HttpEntity savePurchaseWaitingTime(PurchaseWaitingTimeDto purchaseWaitingTime);

    void deletePurchaseWaitingTimeById(UUID id);
}
