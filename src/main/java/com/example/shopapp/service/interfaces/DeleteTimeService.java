package com.example.shopapp.service.interfaces;

import com.example.shopapp.dto.DeleteTimeDto;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface DeleteTimeService {
    HttpEntity getAllDeleteTime(int size, int page, String search, String sort, boolean direction);

    HttpEntity getDeleteTimeById(UUID id);

    HttpEntity saveDeleteTime(DeleteTimeDto deleteTime);

    void deleteDeleteTimeById(UUID id);
}
