package com.example.shopapp.service.interfaces;

import com.example.shopapp.dto.BrandDto;
import com.example.shopapp.model.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface BrandService {
    HttpEntity getAllBrand(int size, int page, String search, String sort, boolean direction);

    HttpEntity getBrandById(UUID id);

    HttpEntity saveBrand(BrandDto brand);

    void deleteBrandById(UUID id);
}
