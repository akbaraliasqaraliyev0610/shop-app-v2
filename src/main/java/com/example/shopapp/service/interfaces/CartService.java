package com.example.shopapp.service.interfaces;

import com.example.shopapp.CartDto;
import com.example.shopapp.model.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface CartService {
    HttpEntity getAllCart(int size, int page, String search, String sort, boolean direction);

    HttpEntity getCartById(UUID id);

    HttpEntity saveCart(CartDto cart);

    void deleteCartById(UUID id);
}
