package com.example.shopapp.service.interfaces;

import com.example.shopapp.dto.AttachmentDto;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface AttachmentService {
    HttpEntity getAllAttachment(int size, int page, String search, String sort, boolean direction);

    HttpEntity getAttachmentById(UUID id);

    HttpEntity saveAttachment(AttachmentDto attachment);

    void deleteAttachmentById(UUID id);
}
