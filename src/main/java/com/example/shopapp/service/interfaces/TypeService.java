package com.example.shopapp.service.interfaces;

import com.example.shopapp.dto.TypeDto;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface TypeService {
    HttpEntity getAllType(int size, int page, String search, String sort, boolean direction);

    HttpEntity getTypeById(UUID id);

    HttpEntity saveType(TypeDto type);

    void deleteTypeById(UUID id);
}
