package com.example.shopapp.service;

import com.example.shopapp.dto.ColorDto;
import com.example.shopapp.service.interfaces.ColorService;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public class ColorServiceImpl implements ColorService {
    @Override
    public HttpEntity getAllColor(int size, int page, String search, String sort, boolean direction) {
        return null;
    }

    @Override
    public HttpEntity getColorById(UUID id) {
        return null;
    }

    @Override
    public HttpEntity saveColor(ColorDto color) {
        return null;
    }

    @Override
    public void deleteColorById(UUID id) {

    }
}
