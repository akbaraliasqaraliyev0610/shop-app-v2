package com.example.shopapp.service;

import com.example.shopapp.dto.DiscountDto;
import com.example.shopapp.service.interfaces.DiscountService;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public class DiscountServiceImpl implements DiscountService {
    @Override
    public HttpEntity getAllDiscount(int size, int page, String search, String sort, boolean direction) {
        return null;
    }

    @Override
    public HttpEntity getDiscountById(UUID id) {
        return null;
    }

    @Override
    public HttpEntity saveDiscount(DiscountDto discount) {
        return null;
    }

    @Override
    public void deleteDiscountById(UUID id) {

    }
}
